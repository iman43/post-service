package service

import (
	"context"
	"database/sql"
	"log"

	pb "github.com/doston/iman/post/genproto"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	grpcclient "github.com/doston/iman/post/service/grpc_client"
	storage "github.com/doston/iman/post/storage"
)

// PostService ...
type PostService struct {
	storage storage.I
	client  grpcclient.IServiceManager
	pb.UnimplementedPostServiceServer
}

// NewPostService ...
func NewPostService(client grpcclient.IServiceManager) *PostService {
	return &PostService{
		storage: storage.NewStorage(),
		client:  client,
	}
}

// CreatePost ...
func (s *PostService) CreatePost(ctx context.Context, req *pb.CreatePostRequest) (*pb.Empty, error) {
	resp, err := s.storage.Post().CreatePost(req)

	if err != nil {
		return nil, err
	}

	return &resp, nil

}

func (s *PostService) GetPostWithId(ctx context.Context, req *pb.GetPostWithIDRequest) (*pb.GetPostWithIDResponse, error) {

	resp, err := s.storage.Post().GetPostWithId(req.Id)

	if err == sql.ErrNoRows {
		log.Println("Error updating post, Not Found")
		return nil, status.Error(codes.NotFound, "Not Found")
	} else if err != nil {
		return nil, err
	}
	return &resp, nil
}

func (s *PostService) GetPosts(ctx context.Context, req *pb.GetPostsRequest) (*pb.GetPostsResponse, error) {

	resp, err := s.storage.Post().GetPosts(req.Page, req.Limit)

	if err != nil {
		return nil, err
	}

	return resp, nil
}

func (s *PostService) UpdatePost(ctx context.Context, req *pb.UpdatePostRequest) (*pb.UpdatePostResponse, error) {
	resp, err := s.storage.Post().UpdatePost(req)
	if err == sql.ErrNoRows {
		log.Println("Error updating post, Not Found")
		return nil, status.Error(codes.NotFound, "Not Found")
	} else if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (s *PostService) DeletePost(ctx context.Context, req *pb.DeletePostRequest) (*pb.Empty, error) {
	err := s.storage.Post().DeletePost(req.Id)

	if err != nil {
		return nil, err
	}

	return &pb.Empty{}, nil
}
