package grpcclient

import (
	"fmt"
	"github.com/doston/iman/post/config"
	postPb "github.com/doston/iman/post/genproto"
	
	"google.golang.org/grpc"
)

//IServiceManager ...
type IServiceManager interface {
	PostService() postPb.PostServiceClient
}

type serviceManager struct {
	cfg *config.Config
	postService postPb.PostServiceClient
}

//New ...
func New(cfg *config.Config) (IServiceManager, error) {

	connPostService, err := grpc.Dial(
		fmt.Sprintf("%s:%d", cfg.PostServiceHost, cfg.PostServicePort),
		grpc.WithInsecure())
	if err != nil {
		return nil, fmt.Errorf("post service dial host: %s port: %d",
			cfg.PostServiceHost, cfg.PostServicePort)
	}

	serviceManager := &serviceManager{
		cfg: cfg,
		postService: postPb.NewPostServiceClient(connPostService),
	}

	return serviceManager, nil
}

// PostService ...
func (s *serviceManager) PostService() postPb.PostServiceClient {
	return s.postService
}
