package postgres

import (
	"database/sql"
	"fmt"
	"log"
	"strings"

	"github.com/jmoiron/sqlx"
	// "google.golang.org/protobuf/internal/errors"

	pb "github.com/doston/iman/post/genproto"
	"github.com/doston/iman/post/helper"
	postgres "github.com/doston/iman/post/platform"
	"github.com/doston/iman/post/storage/repo"
)

type postRepo struct {
	db *sqlx.DB
}

// NewPostRepo ...
func NewPostRepo() repo.PostRepository {

	return &postRepo{
		db: postgres.DB(),
	}
}

func (ur *postRepo) CreatePost(post *pb.CreatePostRequest) (pb.Empty, error) {

	// Transaction begins
	tx, err := ur.db.Beginx()
	if err != nil {
		return pb.Empty{}, err
	}

	defer func() {
		if err != nil {
			_ = tx.Rollback()
			return
		}
		err = tx.Commit()
	}()

	queryStr := `INSERT INTO posts (id, user_id, title, body) VALUES `

	vals := []interface{}{}

	for _, p := range post.Posts {
		queryStr += `(?, ?, ?, ?),`

		vals = append(
			vals,
			p.Id,
			p.UserId,
			p.Title,
			p.Body,
		)
	}

	queryStr = strings.TrimSuffix(queryStr, `,`)

	queryStr = helper.ReplaceSQL(queryStr, `?`)
	
	// prepare the statement
	stmt, err := tx.Prepare(queryStr)

	if err != nil {
		fmt.Printf("error %v", err)
		return pb.Empty{}, err
	}

	// format all vals at once
	_, err = stmt.Exec(vals...)

	if err != nil {
		log.Println(err.Error())
		return pb.Empty{}, nil
	}
	return pb.Empty{}, nil
}

func (ur *postRepo) GetPostWithId(postID int64) (pb.GetPostWithIDResponse, error) {

	var userID int64
	var title string
	var body string

	req := `SELECT user_id, title, body FROM posts WHERE id=$1`
	row := ur.db.QueryRow(req, postID)

	err := row.Scan(&userID, &title, &body)

	if err != nil {
		fmt.Printf("error %v", err)
		return pb.GetPostWithIDResponse{}, err
	}

	return pb.GetPostWithIDResponse{Id: postID, UserId: userID, Title: title, Body: body}, nil
}

func (ur *postRepo) GetPosts(page, limit int32) (*pb.GetPostsResponse, error) {

	posts := make([]*pb.Post, 0, limit)
	offset := (page - 1) * limit

	req := `SELECT 
		p.id,
		p.user_id,
		p.title,
		p.body
	FROM posts p
	LIMIT $1
	OFFSET $2`
	rows, err := ur.db.Query(req, limit, offset)

	if err != nil {
		fmt.Println("Error occured when get all users", err.Error())
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var post pb.Post

		err := rows.Scan(&post.Id, &post.UserId, &post.Title, &post.Body)

		if err != nil {
			log.Fatalf("Unable to scan row. %v", err)
		}

		posts = append(posts, &post)
	}

	return &pb.GetPostsResponse{Posts: posts}, nil
}

func (ur *postRepo) UpdatePost(p *pb.UpdatePostRequest) (pb.UpdatePostResponse, error) {

	sqlStatement := `
	UPDATE posts SET user_id=$2, title=$3, body=$4 WHERE id=$1
	`
	res, err := ur.db.Exec(sqlStatement, p.Id, p.UserId, p.Title, p.Body)
	if err != nil {
		log.Println("Unable to execute query.", err)
	}

	rowsAffected, err := res.RowsAffected()
	if rowsAffected == 0 {
		log.Println("Rows affected.", err)
		return pb.UpdatePostResponse{}, sql.ErrNoRows
	}

	if err != nil {
		log.Println("Rows affected err != nil.", err)
		return pb.UpdatePostResponse{}, err
	}

	return pb.UpdatePostResponse{Id: p.Id}, nil
}

func (ur *postRepo) DeletePost(postID int64) error {

	query := `DELETE from posts where id=$1`
	_, err := ur.db.Exec(query, postID)

	if err != nil {
		log.Fatalf("Rows affected err != nil. %v", err)
	}

	return nil
}
