package repo

import (
	"errors"

	pb "github.com/doston/iman/post/genproto"
)

var (
	// ErrAlreadyExists ...
	ErrAlreadyExists = errors.New("already exists")
	// ErrInvalidField ...
	ErrInvalidField = errors.New("incorrect field")
)

// UserRepository is an interface for client storage
type PostRepository interface {
	Reader
	Writer
}

// Reader - this interface is used for selecting a data
type Reader interface {

	//the performance of the program is one the interesting parts of the Go. If the data being returned is smaller than 1 megabyte, it is actually
	//slower to return a pointer type. So, if the data is less than 1 megabyte it is best to return a value type. These are based on what I read on book called
	//Learning Go. Here it is highly likely that the post won't get bigger than a megabyte. Thus I am considering one post to be smaller than 1 megabyte.
	
	GetPostWithId(postID int64) (pb.GetPostWithIDResponse, error)
	GetPosts(page, limit int32) (*pb.GetPostsResponse, error)
}

// Writer - this interface is used for inserting a data
type Writer interface {
	CreatePost(post *pb.CreatePostRequest) (pb.Empty, error)
	UpdatePost(post *pb.UpdatePostRequest) (pb.UpdatePostResponse, error)
	DeletePost(postID int64) error
}
