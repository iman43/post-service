package storage

import (
	"github.com/doston/iman/post/storage/postgres"
	"github.com/doston/iman/post/storage/repo"
)

// I is an interface for storage
type I interface {
	Post() repo.PostRepository
	
}

type storage struct {
	postRepo repo.PostRepository
}

// NewStorage ...
func NewStorage() I {
	return &storage{
		postRepo: postgres.NewPostRepo(),
	}
}

func (s storage) Post() repo.PostRepository {
	return s.postRepo
}
