package main

import (
	"fmt"
	"net"

	"github.com/doston/iman/post/config"
	"github.com/doston/iman/post/service"

	_ "github.com/joho/godotenv/autoload"
	_ "github.com/lib/pq"
	pb "github.com/doston/iman/post/genproto"
	grpcclient "github.com/doston/iman/post/service/grpc_client"
	"google.golang.org/grpc"
)

func main() {
	// Config
	cfg := config.Get()

	listen, err := net.Listen("tcp", cfg.RPCPort)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	client, err := grpcclient.New(cfg)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	postService := service.NewPostService(client)
	server := grpc.NewServer()
	pb.RegisterPostServiceServer(server, postService)

	if err := server.Serve(listen); err != nil {
		fmt.Println("error listening:", err.Error())
	}

}
