package config

import (
	"os"
	"sync"

	_ "github.com/joho/godotenv/autoload" // load .env file automatically
	"github.com/spf13/cast"
)

//Config ...
type Config struct {
	PostgresHost     string
	PostgresPort     int
	PostgresDatabase string
	PostgresUser     string
	PostgresPassword string
	LogLevel         string
	RPCPort          string
	PostServiceHost  string
	PostServicePort  int
}

func load() *Config {
	return &Config{
		PostgresHost:     cast.ToString(getOrReturnDefault("POSTGRES_HOST", "localhost")),
		PostgresPort:     cast.ToInt(getOrReturnDefault("POSTGRES_PORT", 5432)),
		PostgresDatabase: cast.ToString(getOrReturnDefault("POSTGRES_DB", "")),
		PostgresUser:     cast.ToString(getOrReturnDefault("POSTGRES_USER", "fuser")),
		PostgresPassword: cast.ToString(getOrReturnDefault("POSTGRES_PASSWORD", "")),
		RPCPort:          cast.ToString(getOrReturnDefault("RPC_PORT", ":9000")),
		PostServiceHost:  cast.ToString(getOrReturnDefault("POST_SERVICE_HOST", "127.0.0.1")),
		PostServicePort:  cast.ToInt(getOrReturnDefault("POST_SERVICE_PORT", 9001)),
	}
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}

	return defaultValue
}

var (
	instance *Config
	once     sync.Once
)

//Get ...
func Get() *Config {
	once.Do(func() {
		instance = load()
	})

	return instance
}
